# Clojure Blog

Este mas que un blog es una __libreta de anotaciones__ acerca del aprendizaje y uso del
lenguaje de programación __clojure__.

Las sitas, anotaciones, resúmenes, etc. que provengan de fuentes externas siempre
se tratara de hacer referencia a sus fuentes originales, si alguna referencia falta
notificarlo y se agregara a la brevedad, en caso de que algún contenido
viole algún derecho o licencia de igual manera notificarlo y sera retirado.

_Honor a quien honor merece_

El blog es generado con __cryogen__.

El blog pretende ser una fuente de consulta y referencia personal, así que el
blog contendrá contenido en ingles y en español, y por tiempo podrá contener
contenido copy/paste, no me preocupare mucho por la ortografía y gramática en
los dos idiomas, ademas, ya que no soy un hablante nativo en ingles habrá errores
en la dicción de este lenguaje.

Aunque el blog sea personal igual espero que pueda ser de ayuda a mas persona.

Este blog pretende ser una encapsulanción de los conocimientos adquiridos sobre
el lenguaje de programación clojure y que sirva como un medio para futuras
referencias.
